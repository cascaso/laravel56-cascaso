<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'name' => 'lolxd',
            'author' => 'SKT TELECOM',
            'space_id' => 1,
        ]);
        DB::table('books')->insert([
            'name' => 'ORIGEN',
            'author' => 'xPeke',
            'space_id' => 1,
        ]);
        DB::table('books')->insert([
            'name' => 'G2vodafone',
            'author' => 'Oceote',
            'space_id' => 2,
        ]);
        DB::table('books')->insert([
            'name' => 'Royal',
            'author' => 'japan',
            'space_id' => 1,
        ]);
        DB::table('books')->insert([
            'name' => 'Tigers',
            'author' => 'sharin',
            'space_id' => 3,
        ]);
        factory(App\Book::class, 15)->create();
    }
}
