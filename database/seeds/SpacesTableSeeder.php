<?php

use Illuminate\Database\Seeder;

class SpacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('spaces')->insert([
            'location' => 'pasillo1',
        ]);
        DB::table('spaces')->insert([
            'location' => 'pasillo2',
        ]);
        DB::table('spaces')->insert([
            'location' => 'pasillo3',
        ]);
        DB::table('spaces')->insert([
            'location' => 'pasillo4',
        ]);
    }
}
