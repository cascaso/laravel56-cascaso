<?php

use Illuminate\Database\Seeder;

class BooksUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books_users')->insert([
            'user_id' => 1,
            'book_id' => 1
        ]);
        DB::table('books_users')->insert([
            'user_id' => 1,
            'book_id' => 2
        ]);
        DB::table('books_users')->insert([
            'user_id' => 1,
            'book_id' => 3
        ]);
        DB::table('books_users')->insert([
            'user_id' => 2,
            'book_id' => 1
        ]);
        DB::table('books_users')->insert([
            'user_id' => 2,
            'book_id' => 2
        ]);
    }
}
