<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">

    <h1>Lista prestamos</h1>

    <table class="table table-bordered">
        <tr>
            <th>Nombre</th>
            <th>Autor</th>
        </tr>
    @foreach ($books as $book)
    <tr>
        <td>{{ $book->book->name }}</td>
        <td>{{ $book->book->author }}</td>
    </tr>
    @endforeach
    </table>
    </div>

</body>
</html>
