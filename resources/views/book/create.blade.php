@extends('layouts.app')

@section('content')
<div class="container">

<h1>Crear Libro</h1>

<form method="post" action="/books">
    {{ csrf_field() }}

    <div  class="form-group">
        <label>Nombre</label>
        <input class="form-control"  type="text" name="name" value="{{ old('name') }}">
        @if ($errors->first('name'))
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Autor</label>
        <input class="form-control"  type="text" name="author" value="{{ old('author') }}">
        @if ($errors->first('author'))
            <div class="alert alert-danger">
                {{ $errors->first('author') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Localizacion</label>
        <select class="form-control" name="space_id">
            @foreach ($locations as $location)
                <option value="{{$location->id}}">{{$location->location}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>



</form>

</div>
@endsection
