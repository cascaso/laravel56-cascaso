@extends('layouts.app')

@section('content')
<div class="container">

<h1>Detalle de libro</h1>

<table class="table table-bordered">
    <tr>
        <th>Nombre</th>
        <th>Autor</th>
        <th>Localizacion</th>
    </tr>
    <tr>
        <td>{{ $book->name }}</td>
        <td>{{ $book->author }}</td>
        <td>{{ $book->space->location }}</td>
    </tr>
</table>

</div>
@endsection
