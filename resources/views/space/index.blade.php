@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Espacios</h1>

    <table class="table table-bordered">
    <tr>
        <th>Lugar</th>
        <th>Opciones</th>
    </tr>
    @foreach ($spaces as $space)
    <tr>
        <td>{{ $space->location }}</td>
        <td>
            <a href="/spaces/{{ $space->id }}">Ver</a> -
            <a href="/spaces/{{ $space->id }}/edit">Editar</a>

            <form method="post" action="/spaces/{{ $space->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>
        </td>
    </tr>
    @endforeach
</table>
<a href="/spaces/create">Nuevo</a>
{{ $spaces->links() }}
</div>
@endsection
