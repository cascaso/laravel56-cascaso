@extends('layouts.app')

@section('content')

<div class="container">

<h1>Edicion de usuario</h1>

<form method="post" action="/users/{{ $user->id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">

    <div  class="form-group">
        <label>Nombre</label>
        <input class="form-control"  type="text" name="name" value="{{ $user->name }}">
        @if ($errors->first('name'))
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>
    <div  class="form-group">
        <label>Email</label>
        <input class="form-control"  type="text" name="email" value="{{ $user->email }}">
        @if ($errors->first('email'))
            <div class="alert alert-danger">
                {{ $errors->first('email') }}
            </div>
        @endif
    </div>
    <div  class="form-group">
        <label>Role</label>
        <select class="form-control" name="role_id">
            @foreach ($roles as $role)
                @if($user->role_id == $role->id)
                    <option selected="selected" value="{{$role->id}}">{{$role->name}}</option>
                @else
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endif
            @endforeach
        </select>
        @if ($errors->first('role'))
            <div class="alert alert-danger">
                {{ $errors->first('role') }}
            </div>
        @endif
    </div>
    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Editar">
    </div>

</form>

</div>
@endsection
