@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Usuarios</h1>

<table class="table table-bordered">
    <tr>
        <th>Nombre</th>
        <th>Email</th>
        <th>Role</th>
        <th>Prestamos</th>
        <th>WhishList</th>
        <th>Opciones</th>
    </tr>
@foreach ($users as $user)
<tr>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->role->name }}</td>
    <td>
        <ul>
            @foreach ($user->borrows as $borrow)
            <li>
                {{ $borrow->book->name }}
            </li>
            @endforeach
        </ul>
    </td>
    <td>
        <ul>
            @foreach ($user->books as $book)
            <li>
                {{ $book->name }} - <a href="/users/{{$book->id}}/book">Quitar</a>
            </li>
            @endforeach
        </ul>
    </td>
    <td>
        <a href="/users/{{ $user->id }}">Ver</a> -
        <a href="/users/{{ $user->id }}/edit">Editar</a>

        <form method="post" action="/users/{{ $user->id }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="delete">
            <input type="submit" value="borrar">
        </form>
    </td>
</tr>
@endforeach
</table>
</div>
@endsection

