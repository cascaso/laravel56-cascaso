<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Space;

class SpaceController extends Controller
{
    public function index()
    {
        return Space::all();
    }
    public function show($id)
    {
        // $space = Space::find($id);
        $space = Space::with('books')->find($id);
        if ($space) {
            return $space;
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
    public function update($id)
    {
        $space = Space::find($id);
        if ($space) {
            $space->fill($request->all());
            $space->save();
            return ['update' => $id];
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
    public function destroy($id)
    {
        $space = Space::find($id);
        if ($space) {
            $space->delete();
            return ['delete' => $id];
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
    public function store(Request $request)
    {
        $space = new Space();
        $space->fill($request->all());
        $space->location = $request->input('location');
        $space->save();
        return ['done' => true];
    }
    public function edit($id)
    {
        $space = Space::where('id', $id)->first();
        if ($space) {
            return ['this space' => $id];
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
}
