<?php

namespace App\Http\Controllers;

use App\Book;
use App\Space;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class bookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$books = Book::all();
        $books = Book::paginate(8);
        return view('book.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $spaces = Space::all();
        return view('book.create',  ['locations' => $spaces]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'author' => 'required|max:255'
        ]);
        $this->authorize('create', App\Book::class);
        $book = new Book();
        $book->fill($request->all());
        $book->save();
        return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);
        $this->authorize('view', $book);
        return view('book.show', ['book' => $book]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $spaces = Space::all();
        $book = Book::where('id', $id)->first();
        $this->authorize('update', $book);
        return view('book.edit', ['book' => $book,'locations' => $spaces]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'author' => 'required|max:255'
        ]);
        $book = Book::find($id);
        $this->authorize('update', $book);
        $book->fill($request->all());
        $book->save();
        return redirect('/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $this->authorize('delete', $book);
        if (! $book->borrow) {
            $book->delete();
        }
        return redirect('/books');
    }
}
