<?php

namespace App\Policies;

use App\User;
use App\Borrow;
use Illuminate\Auth\Access\HandlesAuthorization;

class BorrowPolicy
{
    use HandlesAuthorization;


    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }
    /**
     * Determine whether the user can view the borrow.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow  $borrow
     * @return mixed
     */
    public function view(User $user, Borrow $borrow)
    {
        return true;
    }

    /**
     * Determine whether the user can create borrows.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the borrow.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow  $borrow
     * @return mixed
     */
    public function update(User $user, Borrow $borrow)
    {
        //
    }

    /**
     * Determine whether the user can delete the borrow.
     *
     * @param  \App\User  $user
     * @param  \App\Borrow  $borrow
     * @return mixed
     */
    public function delete(User $user, Borrow $borrow)
    {
        //
    }
}
