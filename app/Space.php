<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    protected $fillable = ['location'];
    public function books()
    {
        return $this->hasMany('App\Book');
    }
}
